/*
 *  nautilus_sfshare.c
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Jan Lipovsky <janlipovsky@gmail.com>
 */

#include <glib.h>
/* Nautilus extension headers */
#include <libnautilus-extension/nautilus-menu-provider.h>

#include <stdlib.h>

static GType provider_types[1];
static GType sfshare_type;
static GObjectClass *parent_class;

typedef struct
{
    GObject parent_slot;
} SFShareExtension;

typedef struct
{
    GObjectClass parent_slot;
} SFShareExtensionClass;

/* nautilus extension interface */
void nautilus_module_initialize (GTypeModule  *module);
void nautilus_module_shutdown (void);
void nautilus_module_list_types (const GType **types, int *num_types);
GType sfshare_extension_get_type (void);

static void sfshare_extension_register_type (GTypeModule *module);

/* menu filler */
static GList * sfshare_extension_get_file_items (NautilusMenuProvider   *provider,
                                                 GtkWidget              *window,
                                                 GList                  *files);


/* command callback */
static void sfshare_start_gui (NautilusMenuItem *item, gpointer user_data);

void
nautilus_module_initialize (GTypeModule  *module)
{
    sfshare_extension_register_type (module);

    provider_types[0] = sfshare_extension_get_type ();
}

void
nautilus_module_shutdown (void)
{
    /* Any module-specific shutdown */
}

void
nautilus_module_list_types (const GType **types,
                                 int *num_types)
{
    *types = provider_types;
    *num_types = G_N_ELEMENTS (provider_types);
}

GType
sfshare_extension_get_type (void)
{
    return sfshare_type;
}

static void
sfshare_extension_instance_init (SFShareExtension *object)
{
}

static void
sfshare_extension_class_init (SFShareExtensionClass *class)
{
    parent_class = g_type_class_peek_parent (class);
}

static void
sfshare_extension_menu_provider_iface_init (NautilusMenuProviderIface *iface)
{
    iface->get_file_items = sfshare_extension_get_file_items;
}

/**
* Register new type
*/
static void
sfshare_extension_register_type (GTypeModule *module)
{
    static const GTypeInfo info =
    {
        sizeof (SFShareExtensionClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) sfshare_extension_class_init,
        NULL,
        NULL,
        sizeof (SFShareExtension),
        0,
        (GInstanceInitFunc) sfshare_extension_instance_init,
    };

    static const GInterfaceInfo menu_provider_iface_info =
    {
        (GInterfaceInitFunc) sfshare_extension_menu_provider_iface_init,
        NULL,
        NULL
    };

    sfshare_type = g_type_module_register_type (module,
                         G_TYPE_OBJECT,
                         "SFShareExtension",
                         &info, 0);

    g_type_module_add_interface (module,
                                 sfshare_type,
                                 NAUTILUS_TYPE_MENU_PROVIDER,
                                 &menu_provider_iface_info);
}

/**
* Add item to folder's context menu
*/
static GList *
sfshare_extension_get_file_items (  NautilusMenuProvider    *provider,
                                    GtkWidget               *window,
                                    GList                   *files)
{
    NautilusMenuItem *item;
    NautilusFileInfo *file;
    gchar *name;


    /* Only one folder must be selected */
    if (g_list_length (files) != 1) {
        return NULL;
    }

    /* Only folders can be shared */
    file = NAUTILUS_FILE_INFO (files->data);
    if (!nautilus_file_info_is_directory (file))
    {
        return NULL;
    }

    name = nautilus_file_info_get_name (file);

    item = nautilus_menu_item_new ("SFShareExtension::do_stuff",
                                    g_strconcat("Share \"", name, "\" folder", NULL),
                                    g_strconcat("Show share setup dialog for \"", name, "\" folder", NULL),
                                    NULL /* icon name */);

    g_signal_connect (item, "activate",
                      G_CALLBACK (sfshare_start_gui),
                      provider);

    g_object_set_data ((GObject*) item,
                        "sfshare_extension_folder",
                        NAUTILUS_FILE_INFO (files->data));

    g_free (name);

    return g_list_append (NULL, item);
}


/**
* Run Simple File Share GUI
*/
static void
sfshare_start_gui (NautilusMenuItem *item,
                   gpointer         user_data)
{

        NautilusFileInfo *file = g_object_get_data ((GObject *) item, "sfshare_extension_folder");
        gchar *path;
        GFile *f;

        const gchar *gui_argv[3];
        GError *error;

        f = nautilus_file_info_get_location (file);
        path = g_file_get_path (f);

        gui_argv[0] = BINDIR "/sfshare-gui";
        gui_argv[1] = path; /* Path to selected directory */
        gui_argv[2] = NULL;

        error = NULL;
        g_spawn_async (NULL,        /* working_directory */
                       (gchar **) gui_argv,
                       NULL,        /* envp */
                       0,           /* flags */
                       NULL,        /* child_setup */
                       NULL,        /* user_data */
                       NULL,        /* child_pid */
                       &error);

        if (error != NULL)
        {
                g_warning ("Error launching sfshare-gui: %s", error->message);
                g_error_free (error);
        }

        /* free */
        g_object_unref (f);
        g_object_unref (file);
        g_free (path);

}
