/*
 *  sfshare_errors.c
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Jan Lipovsky <janlipovsky@gmail.com>
 */

#ifndef SFSHARE_ERRORS_H
#define SFSHARE_ERRORS_H


/** Types of errors */
typedef enum
{
    OK,
    ERROR_FAILED,
    ERROR_PERMISSION_DENIED,

    /* Samba share */
    ERROR_FILE_NOT_EXIST,
    ERROR_CAN_NOT_OPEN_FILE,
    ERROR_CAN_NOT_WRITE_TO_FILE,
    ERROR_DIRECTORY_NOT_SHARED,
    ERROR_WRONG_NAME,
    ERROR_WRONG_PATH,
    ERROR_READONLY_WRITABLE,
    ERROR_SHARE_NAME_EXIST,

    NUM_ERRORS
} Error_sfshare;


/** Definition in file: dbus_service.c  */
gchar *get_error_msg (Error_sfshare err);

#endif
