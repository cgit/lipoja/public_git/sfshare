/*
 *  samba_share.h
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Jan Lipovsky <janlipovsky@gmail.com>
 */

#ifndef SAMBA_SHARE_H
#define SAMBA_SHARE_H

#include <glib.h>


typedef struct smb_conf_item
{
    GString *name;          /* [share name] */
    GString *path;          /* path = /path/to/folder */
    GString *comment;       /* comment = Some text comment */
    GString *read_only;     /* read only = yes | no */
    GString *writable;      /* writable = yes | no  ... writeable, write ok */
    GString *guest_ok;      /* guest ok = yes | no */
} TSmbConfItem;


/** Funciton loads all share section to array */
gint load_smb_conf (GPtrArray *shared_items);

/** Function erase shared section containing path from smb.conf */
gint delete_share (GPtrArray *shared_items, const gchar *path);

/** Write new share section or change chare section defined by share parameter */
gint write_share (GPtrArray *shared_items, TSmbConfItem *share);

/** If path is shared then returns share parameters in result. */
gboolean smb_get_share_status (const gchar *path, gchar ***result);

/** Write or channge section in smb.conf */
gint smb_set_share (gchar **parameters);

/** Delete share section from smb.conf */
gint smb_delete_share (gchar **path);

void item_to_strv (TSmbConfItem *item, gchar ***ret);


/** Function changes path to smb.conf to path */
void set_smbconf_path (const gchar *path);


/** Function returns new array */
GPtrArray* shared_items_array_new();

/** Function destroy Share Items Array */
void shared_items_array_free (GPtrArray *array);

/** Function allocs memory for TSmbConfItem and sets parameters */
TSmbConfItem* smbconf_item_new (const gchar *name, const gchar *path, const gchar *comment, const gchar *read_only, const gchar *guest_ok);

/** Function allocs memory for TSmbConfItem */
TSmbConfItem* smbconf_item_new0 ();


/** If ok return 0 else return number of error */
gint check_item (TSmbConfItem *item);

/** Function free memory */
void smbconf_item_free (TSmbConfItem *item);

/** Send SIGHUP to smb and nmb */
void smb_reload_service ();
#endif
