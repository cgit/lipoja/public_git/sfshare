/*
 *  dbus_service.c
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Jan Lipovsky <janlipovsky@gmail.com>
 */

#ifndef DBUS_SERVICE_H
#define DBUS_SERVICE_H

#include <glib.h>
#include <dbus/dbus-glib-lowlevel.h>



typedef void (*AuthorizedCallback)  (void *data);

typedef struct
{
    DBusGMethodInvocation *context;
    gchar  **in;
    AuthorizedCallback authorized_cb;

} DaemonData;

/** Daemon start */
int dbus_sfshare_start ();

GQuark get_error_quark (void);
#define ERROR_QUARK get_error_quark ()



#endif
