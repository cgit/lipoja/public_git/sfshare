/*
 *  dbus_client.c
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Jan Lipovsky <janlipovsky@gmail.com>
 */

#include <glib.h>
#include <gtk/gtk.h>
#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>
#include <stdio.h>
#include <stdlib.h>

#include <polkitgtk/polkitgtk.h>

#include "dbus_client.h"
#include "sfshare.h"
#include "dbus_client_glue.h"

#define SFSHARE_DBUS_NAME "org.fedoraproject.SimpleFileShare"
#define SFSHARE_DBUS_PATH "/org/fedoraproject/SimpleFileShare"
#define SFSHARE_DBUS_INTERFACE "org.fedoraproject.SimpleFileShare"

/* Dbus */
DBusGConnection *bus;
DBusGProxy *remote_object;
gboolean is_dbus_connected = FALSE;
gboolean is_authorized = FALSE;

/* Polkit */
PolkitSubject *subject;
PolkitAuthority *authority;


/** Connect to DBUS */
void
dbus_sfshare_connect ( void )
{
    if(!is_dbus_connected)
    {
        GError *error = NULL;

        bus = dbus_g_bus_get (DBUS_BUS_SYSTEM, &error);
        if (!bus)
        {
            is_dbus_connected = FALSE;

            show_message (GTK_MESSAGE_WARNING, "Couldn't connect to session bus: %s", error->message, "%s", "error->message");
            g_warning ("Couldn't connect to session bus : %s", error->message);

            g_error_free (error);
            return;
        }

        remote_object = dbus_g_proxy_new_for_name (bus,
                        SFSHARE_DBUS_NAME,
                        SFSHARE_DBUS_PATH,
                        SFSHARE_DBUS_INTERFACE);

        is_dbus_connected = TRUE;
    }
}


/** Disconnect from DBUS - free object */
void
dbus_sfshare_disconnect ( void )
{
    if ( is_dbus_connected )
    {
        g_object_unref (G_OBJECT (remote_object));
        is_dbus_connected = FALSE;
    }
}

/** Get Polkit Authority */
void
polkit_sfshare_init ()
{
    authority = polkit_authority_get ();
}

/** Free Polkit objects */
void
polkit_sfshare_free ()
{
    g_object_unref (authority);
    g_object_unref (subject);
}

/** PolicyKit callback */
static void
check_authorization_cb (PolkitAuthority *authority,
                        GAsyncResult    *res,
                        gpointer        *data)
{
    GError *error;
    PolkitAuthorizationResult *result;

    is_authorized = FALSE;

    error = NULL;
    result = polkit_authority_check_authorization_finish (authority, res, &error);
    if (error != NULL)
    {
        g_print ("Error checking authorization: %s\n", error->message);
        g_error_free (error);
    }
    else
    {
        if (polkit_authorization_result_get_is_authorized (result))
        {
            is_authorized = TRUE;
            save_share ();
        }
        else if (polkit_authorization_result_get_is_challenge (result))
        {
            is_authorized = FALSE;
        }
        else
        {
            is_authorized = FALSE;
        }
    }
}

/** Check privileges vie PolicyKit */
void
polkit_sfshare_check  (const gchar *action_id)
{

    subject = polkit_system_bus_name_new (dbus_bus_get_unique_name (dbus_g_connection_get_connection (bus)));

    polkit_authority_check_authorization (authority,
                                          subject,
                                          action_id,
                                          NULL, /* PolkitDetails */
                                          POLKIT_CHECK_AUTHORIZATION_FLAGS_ALLOW_USER_INTERACTION,
                                          NULL,
                                          (GAsyncReadyCallback) check_authorization_cb,
                                          NULL);
}

/** Call daemon D-Bus methot get_share_status */
int
dbus_sfshare_get_share (const gchar *path, gchar ***result)
{
    GError *error = NULL;

    dbus_sfshare_connect ();

    if (!org_fedoraproject_SimpleFileShare_get_share_status (remote_object, path, result, &error))
    {
        show_message (GTK_MESSAGE_WARNING, "%s", "Warning!", "Remote method get_share_status failed: %s", error->message);
        g_warning ("Remote method get_share_status failed: %s", error->message);
        g_error_free (error);

        return -1;
    }

    return 0;
}

/** Reply of setup_share */
void
set_share_reply (DBusGProxy *proxy, GError *error, gpointer userdata)
{
    if (error!= NULL)
    {
        show_message (GTK_MESSAGE_WARNING, "%s","Warning!", "An error occured while calling setup_share remote method: %s", error->message);
        g_warning ("An error occured while calling setup_share remote method: %s", error->message);
        g_error_free (error);
        return;
    }

    reload_share_info ();
    show_message (GTK_MESSAGE_INFO, "%s" ,"Folder is now shared!", "%s", "Share section was succesfully writed to smb.conf");
}


/** Async call daemon D-Bus methot setup_share */
void
dbus_sfshare_set_share (const gchar *name, const gchar *path, const gchar *comment, const gchar *read_only, const gchar *guest_ok)
{
    GError *error = NULL;

    const gchar *share[6] = {name, path, comment, read_only, guest_ok, NULL};

    dbus_sfshare_connect ();

    /* Async call of setup_share metod */
    if (!org_fedoraproject_SimpleFileShare_setup_share_async (remote_object, share, set_share_reply, NULL))
    {
        show_message (GTK_MESSAGE_WARNING, "%s", "Warning!", "Remote method setup_share failed: %s", error->message);
        g_warning ("Remote method setup_share failed: %s", error->message);
        g_error_free (error);
        return;
    }
}



/** Reply of delete_share */
void
delete_share_reply (DBusGProxy *proxy, GError *error, gpointer userdata)
{
    if (error!= NULL)
    {
        show_message (GTK_MESSAGE_WARNING, "%s","Warning!", "An error occured while calling delete_share remote method: %s", error->message);
        g_warning ("An error occured while calling delete_share remote method: %s", error->message);
        g_error_free (error);
        return;
    }

    reload_share_info ();

    show_message (GTK_MESSAGE_INFO, "%s", "Share was deleted!", "%s", "Share section was succesfully deleted from smb.conf");
}


/** Async call daemon D-Bus methot delete_share */
void
dbus_sfshare_delete_share (const gchar *path)
{
    GError *error = NULL;

    dbus_sfshare_connect ();

    /* Async call of delete_share metod */
    if (!org_fedoraproject_SimpleFileShare_delete_share_async (remote_object, path, delete_share_reply, NULL))
    {
        show_message (GTK_MESSAGE_WARNING, "%s", "Warning!", "Remote method delete_share failed: %s", error->message);
        g_warning ("Remote method delete_share failed: %s", error->message);
        g_error_free (error);
        return;
    }
}


