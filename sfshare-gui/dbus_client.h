/*
 *  dbus_client.c
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Jan Lipovsky <janlipovsky@gmail.com>
 */

#ifndef DBUS_SERVICE_H
#define DBUS_SERVICE_H

/** Call daemon D-Bus methot get_share_status */
int dbus_sfshare_get_share (const gchar *path, gchar ***result);
/** Async call daemon D-Bus methot setup_share */
void dbus_sfshare_set_share (const gchar *name, const gchar *path, const gchar *comment, const gchar *read_only, const gchar *guest_ok);
/** Async call daemon D-Bus methot delete_share */
void dbus_sfshare_delete_share (const gchar *path);
/** Connect to DBUS */
void dbus_sfshare_connect ( void );
/** Disconnect from DBUS - free object */
void dbus_sfshare_disconnect ( void );


#define ACTION_ID_SETUP_SHARE "org.fedoraproject.SimpleFileShare.setup_share"
#define ASTION_ID_DELETE_SHARE "org.fedoraproject.SimpleFileShare.delete_share"

/** Check privileges vie PolicyKit */
void polkit_sfshare_check (const gchar *action_id);
/** Get Polkit Authority */
void polkit_sfshare_init();
/** Free Polkit objects */
void polkit_sfshare_free();


#endif

