/*
 *  sfshare.h
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Jan Lipovsky <janlipovsky@gmail.com>
 */

#ifndef SFSHARE_H
#define SFSHARE_H

/** Shows error, info, warning dialog  */
void show_message (GtkMessageType type, const gchar *format, gchar *msgtxt, const gchar *format_sec, gchar *msgtxt_sec);

/** Call functions to save/change or delete section */
void save_share ( void );

/** Reload smbd and nmbd */
void reload_share_info ( void );

#endif

