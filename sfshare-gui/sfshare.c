/*
 *  sfshare.c
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Jan Lipovsky <janlipovsky@gmail.com>
 */

#include <gtk/gtk.h>
#include <string.h> /* strlen */

#include "dbus_client.h"
#include <glib.h>

#include <stdio.h>
#include <stdlib.h>


/** Types of share service */
typedef enum share_service
{
    NOTHING_SELECTED = -1,
    DO_NOT_SHARE,
    WIDNOWS_SHARE_SAMBA
} TShare_service;


/** Dialog widgets */
typedef struct _sfsdialog
{
    GtkWidget  *window;

    GtkWidget *gui_close;
    GtkWidget *gui_save;

    /* Share informations */
    GtkWidget *gui_share;
    GtkWidget *gui_sharename;
    GtkWidget *gui_comment;
    GtkWidget *gui_readonly;
    GtkWidget *gui_guestok;


} SFSDialog;

/** Dialog window */
SFSDialog sfsdialog;

/** Is directory shared? */
gboolean shared_smbconf = FALSE;

/** Did user changed something? */
gboolean change_in_gui = FALSE;

/** Path to folder */
gchar *dir_path;


void
sfshare_dialog_destroy ()
{
    gtk_widget_destroy (sfsdialog.gui_guestok);
    gtk_widget_destroy (sfsdialog.gui_readonly);
    gtk_widget_destroy (sfsdialog.gui_comment);
    gtk_widget_destroy (sfsdialog.gui_sharename);
    gtk_widget_destroy (sfsdialog.gui_share);
    gtk_widget_destroy (sfsdialog.gui_save);
    gtk_widget_destroy (sfsdialog.gui_close);
    gtk_widget_destroy (sfsdialog.window);
}


/** On change event - enable Save button */
void
gui_changed ( void )
{
    if (!change_in_gui)
    {
        /* Enale save button */
        gtk_widget_set_sensitive (sfsdialog.gui_save, TRUE);
        change_in_gui = TRUE;
    }
}


/** Show info, error or warning message */
void
show_message (GtkMessageType type, const gchar *format, gchar *msgtxt, const gchar *format_sec, gchar *msgtxt_sec)
{
    GtkWidget *dialog;

    dialog = gtk_message_dialog_new ( GTK_WINDOW (sfsdialog.window),
                                         GTK_DIALOG_DESTROY_WITH_PARENT,
                                         type,
                                         GTK_BUTTONS_OK,
                                         format,
                                         msgtxt);

    gtk_message_dialog_format_secondary_text ( GTK_MESSAGE_DIALOG (dialog),
                                              format_sec,
                                              msgtxt_sec);

    gtk_dialog_run ( GTK_DIALOG  (dialog));

    gtk_widget_destroy (dialog);
}


/**
* Fill in information about shared directory to GUI
*/
void
load_share_info ( void )
{

    /* [share name], "path =", "commennt =", "read only =", "writable =", "guest ok =" */

    gchar **share;
    gboolean readonly = FALSE;
    gboolean guestok = FALSE;
    TShare_service share_type = DO_NOT_SHARE;

    shared_smbconf = FALSE;
    change_in_gui = FALSE;

    /* Disable save button */
    gtk_widget_set_sensitive (sfsdialog.gui_save, FALSE);

    if (!dbus_sfshare_get_share (dir_path, &share))
    {
        /* Folder is not shared */
        if(share[0] == NULL)
        {
            /* Directory is not shared - clear info */
            gtk_entry_set_text( GTK_ENTRY (sfsdialog.gui_sharename), "");
            gtk_entry_set_text( GTK_ENTRY (sfsdialog.gui_comment), "");
            gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (sfsdialog.gui_readonly), FALSE);
            gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (sfsdialog.gui_guestok), FALSE);
            gtk_combo_box_set_active (GTK_COMBO_BOX (sfsdialog.gui_share), DO_NOT_SHARE);

            /* Free */
            g_strfreev(share);
            return;
        }

        share_type = WIDNOWS_SHARE_SAMBA;

        if (!g_strcmp0(share[3], "yes"))
            readonly = TRUE;

        if (!g_strcmp0(share[4], "yes"))
            guestok = TRUE;

        /* Fill in information about shared directory to GUI */
        gtk_entry_set_text( GTK_ENTRY (sfsdialog.gui_sharename), share[0]);
        gtk_entry_set_text( GTK_ENTRY (sfsdialog.gui_comment), share[2]);
        gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (sfsdialog.gui_readonly), readonly);
        gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (sfsdialog.gui_guestok), guestok);
        gtk_combo_box_set_active (GTK_COMBO_BOX (sfsdialog.gui_share), share_type);

        shared_smbconf = TRUE;

        /* Free vector*/
        g_strfreev(share);
    }
}

void
reload_share_info ( void )
{
     /* Reload info */
     load_share_info ();
}

/**
* Check authority via PolicyKit - OnClick Save
*/
void
sfshare_authority_check ( void )
{
    gboolean shared_gui = FALSE;

    if ((gtk_combo_box_get_active (GTK_COMBO_BOX (sfsdialog.gui_share)) != DO_NOT_SHARE) )
        shared_gui = TRUE;


    /* If not shared in smbconf and user do not want to share it nothing changes */
    if (!shared_gui && !shared_smbconf)
        return;

    /* user want to delete share from smb.conf*/
    if (!shared_gui && shared_smbconf)
    {
        polkit_sfshare_check (ASTION_ID_DELETE_SHARE);

        return;
    }

    /* user want to write share to smb.conf*/
    if (shared_gui)
    {
        const gchar *share_name = gtk_entry_get_text ( GTK_ENTRY (sfsdialog.gui_sharename));
        if (!g_strcmp0(share_name, ("")))
        {
            show_message(GTK_MESSAGE_WARNING,"%s","Warning!","%s","Share name must be set!");
            return;
        }

        polkit_sfshare_check (ACTION_ID_SETUP_SHARE);
    }
}



/**
* Save button - call functions to save/change or delete section
*/
void
save_share ( void )
{

    gboolean shared_gui = FALSE;

    if ((gtk_combo_box_get_active (GTK_COMBO_BOX (sfsdialog.gui_share)) != DO_NOT_SHARE) )
        shared_gui = TRUE;

    /* If not shared in smbconf and user do not want to share it nothing changes */
    if (!shared_gui && !shared_smbconf)
        return;


    /* user want to delete share from smb.conf*/
    if (!shared_gui && shared_smbconf)
    {
        dbus_sfshare_delete_share (dir_path);
        return;
    }

    /* user want to write share to smb.conf*/
    if (shared_gui)
    {
        const gchar *yes = "yes";
        const gchar *no = "no";

        const gchar *ronly;
        const gchar *gok;

        const gchar *share_name = gtk_entry_get_text ( GTK_ENTRY (sfsdialog.gui_sharename));
        const gchar *comment = gtk_entry_get_text ( GTK_ENTRY (sfsdialog.gui_comment));

        if (gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (sfsdialog.gui_readonly)))
            ronly = yes;
        else
            ronly = no;

        if (gtk_toggle_button_get_active ( GTK_TOGGLE_BUTTON (sfsdialog.gui_guestok)))
            gok = yes;
        else
            gok = no;

        /* Call dbus method */
        dbus_sfshare_set_share (share_name, dir_path, comment, ronly, gok);
        return;
    }
}

/**
* Create main window
*/
int
main ( int argc,  char **argv )
{

    GtkBuilder *builder;
    const gchar *ui_filename;
    GError *error = NULL;

    if (argc < 2)
    {
        g_warning ("Wrong parameter!");
        exit(1);
    }
    /* First parameter is path to shared folder */
    dir_path = argv[1];

    gtk_init( &argc, &argv );

    if (!g_file_test (dir_path, G_FILE_TEST_IS_DIR))
    {
        g_warning ("Directory \'%s\' does not exist!", dir_path);
        exit(1);
    }


    /* Create builder and load interface */
    builder = gtk_builder_new ();

    ui_filename = DATADIR "/sfshare/sfshare_window.ui";
    if (!g_file_test (ui_filename, G_FILE_TEST_EXISTS))
    	ui_filename = "data/sfshare_window.ui";


    if (!gtk_builder_add_from_file ( builder, ui_filename, &error ))
    {
        g_warning ("gtk_builder_add_from_file failed: %s", error->message);
        g_error_free (error);
        return -1;
    }

    /* Obtain widgets that we need */
    sfsdialog.window = GTK_WIDGET ( gtk_builder_get_object( builder, "sfshare_window" ));
    g_signal_connect (sfsdialog.window, "destroy", gtk_main_quit, NULL);

    sfsdialog.gui_close = GTK_WIDGET( gtk_builder_get_object( builder, "button_close" ));
    g_signal_connect (sfsdialog.gui_close, "clicked", gtk_main_quit, GTK_OBJECT (sfsdialog.window));

    sfsdialog.gui_save = GTK_WIDGET( gtk_builder_get_object( builder, "button_save" ));
    g_signal_connect (sfsdialog.gui_save, "clicked", G_CALLBACK (sfshare_authority_check), NULL);

    /* Share info items */
    sfsdialog.gui_share = GTK_WIDGET( gtk_builder_get_object( builder, "combobox_share"));
    sfsdialog.gui_sharename = GTK_WIDGET( gtk_builder_get_object( builder, "entry_share_name"));
    sfsdialog.gui_comment = GTK_WIDGET( gtk_builder_get_object( builder, "entry_comment"));
    sfsdialog.gui_readonly = GTK_WIDGET( gtk_builder_get_object( builder, "checkbutton_readonly"));
    sfsdialog.gui_guestok = GTK_WIDGET( gtk_builder_get_object( builder, "checkbutton_guestok"));


    g_signal_connect (sfsdialog.gui_share, "changed", G_CALLBACK (gui_changed), NULL);
    g_signal_connect (sfsdialog.gui_sharename, "changed", G_CALLBACK (gui_changed), NULL);
    g_signal_connect (sfsdialog.gui_comment, "changed", G_CALLBACK (gui_changed), NULL);
    g_signal_connect (sfsdialog.gui_readonly, "clicked", G_CALLBACK (gui_changed), NULL);
    g_signal_connect (sfsdialog.gui_guestok, "clicked", G_CALLBACK (gui_changed), NULL);


    /* Destroy builder */
    g_object_unref (G_OBJECT( builder ));

    /* Load info about directory */
    load_share_info ();

    /* Show main window and start main loop */
    gtk_widget_show ( sfsdialog.window );

    /* Dbus and polkit init  */
    dbus_sfshare_connect ();
    polkit_sfshare_init();

    gtk_main ();

    dbus_sfshare_disconnect ();
    polkit_sfshare_free ();

    return(0);
}
