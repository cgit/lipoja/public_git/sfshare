/* Generated by dbus-binding-tool; do not edit! */

#include <glib.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

#ifndef _DBUS_GLIB_ASYNC_DATA_FREE
#define _DBUS_GLIB_ASYNC_DATA_FREE
static
#ifdef G_HAVE_INLINE
inline
#endif
void
_dbus_glib_async_data_free (gpointer stuff)
{
	g_slice_free (DBusGAsyncData, stuff);
}
#endif

#ifndef DBUS_GLIB_CLIENT_WRAPPERS_org_fedoraproject_SimpleFileShare
#define DBUS_GLIB_CLIENT_WRAPPERS_org_fedoraproject_SimpleFileShare

static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_fedoraproject_SimpleFileShare_get_share_status (DBusGProxy *proxy, const char * IN_path, char *** OUT_status, GError **error)

{
  return dbus_g_proxy_call (proxy, "get_share_status", error, G_TYPE_STRING, IN_path, G_TYPE_INVALID, G_TYPE_STRV, OUT_status, G_TYPE_INVALID);
}

typedef void (*org_fedoraproject_SimpleFileShare_get_share_status_reply) (DBusGProxy *proxy, char * *OUT_status, GError *error, gpointer userdata);

static void
org_fedoraproject_SimpleFileShare_get_share_status_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  char ** OUT_status;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_STRV, &OUT_status, G_TYPE_INVALID);
  (*(org_fedoraproject_SimpleFileShare_get_share_status_reply)data->cb) (proxy, OUT_status, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_fedoraproject_SimpleFileShare_get_share_status_async (DBusGProxy *proxy, const char * IN_path, org_fedoraproject_SimpleFileShare_get_share_status_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "get_share_status", org_fedoraproject_SimpleFileShare_get_share_status_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_STRING, IN_path, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_fedoraproject_SimpleFileShare_setup_share (DBusGProxy *proxy, const char ** IN_parameters, GError **error)

{
  return dbus_g_proxy_call (proxy, "setup_share", error, G_TYPE_STRV, IN_parameters, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_fedoraproject_SimpleFileShare_setup_share_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_fedoraproject_SimpleFileShare_setup_share_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_fedoraproject_SimpleFileShare_setup_share_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_fedoraproject_SimpleFileShare_setup_share_async (DBusGProxy *proxy, const char ** IN_parameters, org_fedoraproject_SimpleFileShare_setup_share_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "setup_share", org_fedoraproject_SimpleFileShare_setup_share_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_STRV, IN_parameters, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_fedoraproject_SimpleFileShare_delete_share (DBusGProxy *proxy, const char * IN_path, GError **error)

{
  return dbus_g_proxy_call (proxy, "delete_share", error, G_TYPE_STRING, IN_path, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_fedoraproject_SimpleFileShare_delete_share_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_fedoraproject_SimpleFileShare_delete_share_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_fedoraproject_SimpleFileShare_delete_share_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_fedoraproject_SimpleFileShare_delete_share_async (DBusGProxy *proxy, const char * IN_path, org_fedoraproject_SimpleFileShare_delete_share_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "delete_share", org_fedoraproject_SimpleFileShare_delete_share_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_STRING, IN_path, G_TYPE_INVALID);
}
#endif /* defined DBUS_GLIB_CLIENT_WRAPPERS_org_fedoraproject_SimpleFileShare */

G_END_DECLS
